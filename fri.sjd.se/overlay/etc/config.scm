;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules desktop networking ssh xorg security-token)

(operating-system
  (locale "sv_SE.utf8")
  (timezone "Europe/Stockholm")
  (keyboard-layout (keyboard-layout "se"))
  (host-name "fri")
  (users (cons* (user-account
                  (name "jas")
                  (comment "Simon Josefsson")
                  (group "users")
                  (home-directory "/home/jas")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
     (map specification->package
	  '("nss-certs"
	    "hdparm"
	    "smartmontools"
	    "iotop"
	    "tcpdump"
	    "strace"
	    "rsync"
	    "evolution"
	    "owncloud-client"
	    "ungoogled-chromium"
	    "gnupg"
	    "gnome-tweaks"
	    "emacs"))
     %base-packages))
  (services
    (append
      (list (service gnome-desktop-service-type)
            (service openssh-service-type
		     (openssh-configuration
		      (x11-forwarding? #t)
		      (permit-root-login 'without-password)))
	    (service pcscd-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "aa6e6dea-e6c3-4c03-8493-1fc04b83e8bc"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "ext4")
             (dependencies mapped-devices))
           %base-file-systems)))
