(use-modules (gnu))
(use-service-modules networking ssh)

(use-modules (guix) (gnu) (gnu services mcron))
					;(use-modules (gnu packages))
(use-modules (gnu services admin))
(use-modules (srfi srfi-1))

(define garbage-collector-job
  #~(job "5 0 * * *" "guix gc -F 1G"))

(define backup-daily-job
  #~(job "42 1 * * *" "/backup/backup-all daily"))

(define backup-weekly-job
  #~(job "15 23 * * 3" "/backup/backup-all weekly"))

(define backup-monthly-job
  #~(job "15 17 1 * *" "/backup/backup-all monthly"))

(define backup-yearly-job
  #~(job "23 9 1 1 *" "/backup/backup-all yearly"))

(operating-system
 (locale "sv_SE.utf8")
 (timezone "Europe/Stockholm")
 (keyboard-layout (keyboard-layout "se"))
 (host-name "hamster")

 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets (list "/dev/sda"))
              (keyboard-layout keyboard-layout)))

 (swap-devices (list (swap-space
                      (target (uuid
                               "472feb7c-c859-436e-a9a2-b55db3a8cc17")))))

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "jas")
                (comment "Simon Josefsson")
                (group "users")
                (home-directory "/home/jas")
                (supplementary-groups '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))

 (mapped-devices
  (list (mapped-device
	 (source (list "/dev/sdc1" "/dev/sdd1"))
	 (target "/dev/md1")
	 (type raid-device-mapping))))

 (file-systems (cons* (file-system
                       (mount-point "/")
                       (device (uuid
                                "93656f76-35a3-4395-b07d-8ac28d11e8a3"
                                'ext4))
                       (type "ext4"))
;		      (file-system
;		       (device "/dev/md1")
;		       (mount-point "/backup")
;		       (type "ext4"))
		      %base-file-systems))

 (packages (append (map specification->package
                        '(
			  "emacs-no-x"
			  "glibc-locales"
			  "hdparm"
			  "iotop"
			  "lvm2"
			  "mdadm"
;			  "nss-certs"
			  "openntpd"
			  "openssh-sans-x"
			  "rsnapshot"
			  "rsync"
			  "smartmontools"
			  "tcpdump"
			  ))
		   (lset-difference eq? %base-packages
				    (map specification->package
					 '("zile"
					   "iw"
					   "wireless-tools"
					   "nano")))))

 (services (cons* (service dhcp-client-service-type)

		  (service openntpd-service-type
			   (openntpd-configuration
			    (constraint-from '("www.gnu.org"))
			    (constraints-from '("https://www.google.com/"))))

                  (service openssh-service-type
			   (openssh-configuration
			    (permit-root-login 'prohibit-password)))

		  (service unattended-upgrade-service-type
			   (unattended-upgrade-configuration
			    (schedule "42 05 * * *")
			    (services-to-restart '(mcron ntpd ssh-daemon))))

		  (service mcron-service-type
			   (mcron-configuration
			    (jobs (list backup-daily-job
					backup-weekly-job
					backup-monthly-job
					backup-yearly-job
					garbage-collector-job))))

                  %base-services)))
