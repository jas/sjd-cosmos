# ASRock H510 Pro BTC+ -- Radeon VII -- Ubuntu 20.04 -- gpuowl

# Ubuntu Installation

- Import ssh key
- HWE for Intel I219-v - https://wiki.ubuntu.com/Kernel/LTSEnablementStack
```
sudo apt-get install --install-recommends linux-generic-hwe-20.04
```

# BIOS

- https://asrock.com/MB/Intel/H510%20Pro%20BTC+/index.asp

```
sudo wipefs -a /dev/sdb
sudo parted -s /dev/sdb -- mklabel msdos \
    mkpart primary fat32 4MiB -1s
sudo partprobe /dev/sdb
sudo mkfs.vfat  /dev/sdb1
sudo mount /dev/sdb1  /mnt/
cd /mnt
sudo unzip ~jas/moln/sysadmin/asrock-h510pro-btc+/H510\ Pro\ BTC+\(1.50\)ROM.zip
cd
sudo umount /mnt
sync
```

# ROCM

- https://www.amd.com/en/support/graphics/amd-radeon-2nd-generation-vega/amd-radeon-2nd-generation-vega/amd-radeon-vii
- https://www.mersenneforum.org/showthread.php?t=25601
- https://rocmdocs.amd.com/en/latest/Non-Doxygen_RST/rocm_cli/ROCm-CLI.html?highlight=ppfeaturemask#clock-and-temperature-management
- https://rocmdocs.amd.com/en/latest/Installation_Guide/Installation_new.html#overview-of-rocm-installation-methods

```
sudo usermod -a -G video $LOGNAME
sudo usermod -a -G render $LOGNAME
sudo apt-get update
wget https://repo.radeon.com/amdgpu-install/22.10.2/ubuntu/focal/amdgpu-install_22.10.2.50102-1_all.deb
sudo apt-get install ./amdgpu-install_22.10.2.50102-1_all.deb
sudo apt-get update
sudo apt install rocm-dev
```

# gpuowl

- https://github.com/preda/gpuowl

```
sudo apt install clinfo make libgmp-dev
git clone https://github.com/preda/gpuowl
cd gpuowl
make LIBPATH=-L/opt/rocm-5.2.0/lib
```

# Useful commands

```
sudo tail -F /var/log/syslog /home/jas/gpuowl/nohup.out /home/jas/gpuowl/gpuowl.log &

cd gpuowl/
./tools/primenet.py -u jas -p ... --tasks 1 | logger --tag primenet &
nohup ./gpuowl -maxAlloc 15G -nospin &

killall -INT gpuowl

rocm-smi -a
rocm-smi --setsclk 3
```
