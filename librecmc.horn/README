#!/bin/sh

# ThinkPenguin TPE-R1300 -- horn -- LibreCMC v6.1 -- JOW13 Kontor
# https://gogs.librecmc.org/libreCMC/libreCMC/wiki/TPE_R1300

# CPU:        Qualcomm QCA9531 SoC, 650MHz CPU
# RAM/NOR:    128MB RAM, 16MB Nor flash
# Interface:  1 WAN, 1 LAN, 1 USB 2.0, 1 MicroUSB, UART, reset button
# LED:        1st LED is power and system (solid green when booted); 3rd LED is red and for wifi; the 2nd LED is programmable
# WAN/LAN:    10/100Mbps
# WLAN:       802.11b/g/n

# Wifi Access Point.  Local IP from DHCP.  No WAN.  No local DHCP server.
# OpenSSH and Munin servers.

# Last install 2024-08-27

# Download and verify firmware
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v6.1/targets/ath79/generic/librecmc-ath79-generic-thinkpenguin_tpe-r1300-squashfs-sysupgrade.bin
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v6.1/targets/ath79/generic/sha256sums
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v6.1/targets/ath79/generic/sha256sums.asc
gpg --verify sha256sums.asc
sha256sum --ignore-missing -c sha256sums
mv librecmc-ath79-generic-thinkpenguin_tpe-r1300-squashfs-sysupgrade.bin fw.bin

# Copy to device
scp fw.bin root@192.168.10.6:/tmp

# Sysupgrade it
ssh root@192.168.10.6
sysupgrade -n -v /tmp/fw.bin

# Reach router over switched ethernet as 192.168.10.1/24 on LAN port
sudo ip a add 192.168.10.42/24 dev enp0s25 # wlp2s0

# Wait until SYS led is stable
ping 192.168.10.1

# Login and run commands
ssh-keygen -f "/home/jas/moln/dot-files/ssh-known-hosts" -R "192.168.10.1"
ssh root@192.168.10.1

passwd

echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILzCFcHHrKzVSPDDarZPYqn89H5TPaxwcORgRg+4DagE cardno:FFFE67252015' > /etc/dropbear/authorized_keys

uci set system.@system[0].hostname=horn
uci set system.@system[0].zonename=Europe/Stockholm
uci set system.@system[0].timezone=CET-1CEST,M3.5.0,M10.5.0/3

uci set network.lan.proto=dhcp
uci del network.lan.ipaddr
uci del network.lan.netmask

uci set wireless.@wifi-iface[0].ssid=JOW13
uci set wireless.@wifi-iface[0].encryption=psk2
uci set wireless.@wifi-iface[0].key=xxxxxxxx
uci set wireless.@wifi-iface[0].wpa_disable_eapol_key_retries=1
uci set wireless.@wifi-iface[0].tdls_prohibit=1
uci set wireless.@wifi-device[0].country=SE
uci set wireless.@wifi-device[0].log_level=1
uci del wireless.@wifi-device[0].disabled

/etc/init.d/odhcpd disable
/etc/init.d/dnsmasq disable

uci commit
reboot

# Wait until SYS led is stable
ping 192.168.10.6

# Login to system
ssh-keygen -f "/home/jas/moln/dot-files/ssh-known-hosts" -R "192.168.10.6"
ssh root@192.168.10.6

# Update package list
opkg update

# Reboot to test
uci commit
sync
reboot

# Login to system
ping 192.168.10.6
ssh root@192.168.10.6
dmesg
logread -f &
logread
