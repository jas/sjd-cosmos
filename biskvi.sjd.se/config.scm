;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu))
(use-service-modules cups desktop networking ssh xorg)

;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
(use-service-modules security-token)
(use-package-modules security-token)

(operating-system
  (locale "sv_SE.utf8")
  (timezone "Europe/Stockholm")
  (keyboard-layout (keyboard-layout "se"))
  (host-name "biskvi")

  ;; https://issues.guix.gnu.org/66651
  ;; https://wiki.archlinux.org/title/Intel_graphics
  (kernel-arguments (cons "i915.enable_guc=0" %default-kernel-arguments))

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "jas")
                  (comment "Simon Josefsson")
                  (group "users")
                  (home-directory "/home/jas")
                  (supplementary-groups '("wheel" "netdev" "audio" "video"
;;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
					  "plugdev")))
                %base-user-accounts))

  ;; https://guix.gnu.org/manual/devel/en/guix.html#Name-Service-Switch
  (name-service-switch %mdns-host-lookup-nss)

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list (service gnome-desktop-service-type)
                 (service pcscd-service-type)

;;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
		 (udev-rules-service 'fido2 libfido2 #:groups '("plugdev"))

                 (service openssh-service-type)
                 (service cups-service-type)
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout))))

           ;; This is the default list of services we
           ;; are appending to.
           %desktop-services))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "9210cc11-938d-4c3a-8f97-8c97e8e6317e")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "8A46-8847"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "bfddb98e-dbf7-47ef-b5fa-625ef4615315"
                                  'ext4))
                         (type "ext4")) %base-file-systems)))
