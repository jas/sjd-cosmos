# https://doc.dovecot.org/admin_manual/ssl/dovecot_configuration/
# https://ssl-config.mozilla.org/#server=dovecot&version=2.3.4&config=intermediate&openssl=1.1.1d
ssl=required

# https://doc.dovecot.org/settings/core/#ssl-min-protocol
# Evolution: TLSv1.3
# iPhone Mail: TLSv1.3
# Nextcloud Mail: TLSv1.2
ssl_min_protocol = TLSv1.2

# https://doc.dovecot.org/settings/core/#ssl-cipher-list
ssl_cipher_list = ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384

# https://doc.dovecot.org/settings/core/#ssl-prefer-server-ciphers
ssl_prefer_server_ciphers=yes

disable_plaintext_auth=yes

# https://doc.dovecot.org/settings/core/#login-log-format-elements
# https://www.sidorenko.io/post/2014/02/secure-ssl-configuration-for-apache-postfix-dovecot/
login_log_format_elements = "user=<%u> method=%m rip=%r lip=%l mpid=%e %c %k"

# https://doc.dovecot.org/configuration_manual/authentication/authentication_mechanisms/
auth_mechanisms = scram-sha-256 scram-sha-1 cram-md5 # plain

# https://doc.dovecot.org/configuration_manual/mail_location/
mail_location = maildir:~/Maildir

# Username formatting before it's looked up from databases. You can use
# the standard variables here, eg. %Lu would lowercase the username, %n would
# drop away the domain if it was given, or "%n-AT-%d" would change the '@' into
# "-AT-". This translation is done after auth_username_translation changes.
auth_username_format = %n

first_valid_uid = 100

passdb {
  driver = passwd-file
  args = /etc/dovecot/users
}

userdb {
  driver = static
  args = uid=dovmail gid=dovmail home=/var/spool/dovecot/%d/%u
}

# https://doc.dovecot.org/configuration_manual/acl/
mail_plugins = acl
protocol imap {
  mail_plugins = $mail_plugins imap_acl
}
plugin {
  acl = vfile:/etc/dovecot/dovecot-acl:cache_secs=60
  # If enabled, don't try to find dovecot-acl files from mailbox directories.
  # This reduces unnecessary disk I/O when only global ACLs are used.
  # (v2.2.31+)
  acl_globals_only = yes
}

# https://wiki.dovecot.org/Pigeonhole/Sieve/Configuration
protocol lmtp {
  mail_plugins = $mail_plugins sieve

# https://doc.dovecot.org/settings/core/#lmtp-save-to-detail-mailbox
  lmtp_save_to_detail_mailbox = yes
}

protocol imap {
  # https://doc.dovecot.org/settings/core/#mail-max-userip-connections
  mail_max_userip_connections = 100
}

# https://github.com/nextcloud/server/issues/29333
# https://doc.dovecot.org/admin_manual/submission_server/
#protocol submission {
#  submission_relay_host = pippi.sjd.se
#  submission_relay_ssl = starttls
#}

# https://doc.dovecot.org/admin_manual/debugging/debugging_rawlog/
protocol imap {
  rawlog_dir = %h/rawlog
}
protocol submission {
  submission_relay_rawlog_dir =%h/rawlog
}
# pre-login rawlog
#service imap-login {
#  executable = imap-login -R rawlogs
#}
