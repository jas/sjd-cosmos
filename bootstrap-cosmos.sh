#!/bin/sh

set -e

cmd_hostname="$1"
if test -z "$cmd_hostname"; then
    echo "Usage: $0 HOSTNAME"
    exit 1
fi

set -x

apt-get -y install rsync git gpg wget

if ! test -f /var/cache/apt/archives/cosmos_1.5-2_all.deb; then
    mkdir -p /var/cache/apt/archives
    wget -O /var/cache/apt/archives/cosmos_1.5-2_all.deb https://gitlab.com/jas/sjd-cosmos/-/raw/master/apt/cosmos_1.5-2_all.deb
fi
echo '1acff108bc4913367fce07e2c1daab2528bb07578e9471a36eefb2c0e7013d7f  /var/cache/apt/archives/cosmos_1.5-2_all.deb' | sha256sum -c || exit 1
dpkg -i /var/cache/apt/archives/cosmos_1.5-2_all.deb

if ! test -d /var/cache/cosmos/repo; then
    cosmos clone https://gitlab.com/jas/sjd-cosmos.git
fi

hostname $cmd_hostname

perl -pi -e "s,#COSMOS_REPO_MODELS=.*,COSMOS_REPO_MODELS=\"\\\$COSMOS_REPO/global/:\\\$COSMOS_REPO/$cmd_hostname/\"," /etc/cosmos/cosmos.conf
perl -pi -e 's,#COSMOS_UPDATE_VERIFY_GIT_TAG_PATTERN=.*,COSMOS_UPDATE_VERIFY_GIT_TAG_PATTERN="sjd-cosmos*",' /etc/cosmos/cosmos.conf

COSMOS_BASE=/var/cache/cosmos /var/cache/cosmos/repo/global/pre-tasks.d/010cosmos-trust

cosmos -v update
cosmos -v apply

exit 0
