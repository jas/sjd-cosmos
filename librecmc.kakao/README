#!/bin/sh

# Netgear WNDR3800 -- kakao -- LibreCMC 1.5.13 -- JOW13 Garage
# https://librecmc.org/fossil/librecmc/wiki?name=WNDR3800
# https://gogs.librecmc.org/libreCMC/libreCMC/wiki/WNDR3800
# https://openwrt.org/toh/netgear/wndr3800

# DHCP server for local network.
# WireGuard server Point-to-Site access to local network.
# OpenSSH and Munin servers.

# Last install 2023-04-29

# Download and verify firmware
rm -f sha256sums sha256sums.asc librecmc-*-sysupgrade.bin fw.bin
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v1.5.13/targets/ath79/generic/librecmc-ath79-generic-netgear_wndr3800-squashfs-sysupgrade.bin
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v1.5.13/targets/ath79/generic/sha256sums
wget -nv https://librecmc.org/librecmc/downloads/snapshots/v1.5.13/targets/ath79/generic/sha256sums.asc
gpg --verify sha256sums.asc
sha256sum --ignore-missing --check sha256sums
mv librecmc-ath79-generic-netgear_wndr3800-squashfs-sysupgrade.bin fw.bin

# Copy to device
scp fw.bin root@192.168.23.21:/tmp

# Sysupgrade it
ssh root@192.168.23.21
sysupgrade -n -v /tmp/fw.bin

# Stock firmware factory settings:
# x) Download and verify firmware
#    wget https://librecmc.org/librecmc/downloads/snapshots/v1.5.2/targets/ath79/generic/librecmc-ath79-generic-netgear_wndr3800-squashfs-factory.img
#    wget https://librecmc.org/librecmc/downloads/snapshots/v1.5.2/targets/ath79/generic/sha256sums
#    wget https://librecmc.org/librecmc/downloads/snapshots/v1.5.2/targets/ath79/generic/sha256sums.asc
#    gpg --verify sha256sums.asc
#    sha256sum --ignore-missing --check sha256sums
# x) Connect to WiFi ESSID/password on the back of the device
# x) Connect to 192.168.1.1
# x) Go to Firmware Upgrade and select firmware

# Reach router over switched ethernet on LAN port
sudo ip a add 192.168.10.42/24 dev enp0s25 # wlp2s0

# Wait until SYS led is stable
ping 192.168.10.1

# Login and run commands
ssh-keygen -f "/home/jas/moln/dot-files/ssh-known-hosts" -R "192.168.10.1"
ssh -o HostKeyAlgorithms=+ssh-rsa root@192.168.10.1

passwd

uci set system.@system[0].hostname=kakao
uci set system.@system[0].zonename=Europe/Stockholm
uci set system.@system[0].timezone=CET-1CEST,M3.5.0,M10.5.0/3

uci del network.lan.netmask
uci del network.lan.ipaddr
uci add_list network.lan.ipaddr=192.168.23.21/24

uci commit
reboot

# Reach router over switched ethernet on LAN port
sudo ip a add 192.168.23.42/24 dev enx803f5d0916ac # enp47s0 wlp2s0

# Wait until SYS led is stable green
ping 192.168.23.21

# Login to system
ssh-keygen -f "/home/jas/moln/dot-files/ssh-known-hosts" -R "192.168.23.21"
ssh -o HostKeyAlgorithms=+ssh-rsa root@192.168.23.21

# Work around if only LAN-cable is connected:
ip a add 192.168.10.21/24 dev br-lan
ip route add default via 192.168.10.46 dev br-lan
echo nameserver 192.168.10.46 > /etc/resolv.conf

# Update package list
opkg update

# Munin
opkg install muninlite

# Tcpdump
opkg install tcpdump

# OpenSSH (ed25519)
opkg install openssh-server
echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config
mkdir /root/.ssh
echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILzCFcHHrKzVSPDDarZPYqn89H5TPaxwcORgRg+4DagE cardno:FFFE67252015' > /root/.ssh/authorized_keys
/etc/init.d/sshd enable
/etc/init.d/dropbear disable

# Wireguard -- https://openwrt.org/docs/guide-user/services/vpn/wireguard/server
opkg install wireguard
VPN_IF="vpn"
VPN_PORT="51820"
VPN_ADDR="192.168.9.1/24"
VPN_ADDR6="fd00:9::1/64"
# creds/wg-kakao.asc
VPN_KEY="xxx"
VPN_PSK="yyy"
VPN_PUB="zzz"

# Configure firewall
# https://openwrt.org/docs/guide-user/firewall/firewall_configuration
uci rename firewall.@zone[0]="lan"
uci rename firewall.@zone[1]="wan"
uci del_list firewall.lan.network="${VPN_IF}"
uci add_list firewall.lan.network="${VPN_IF}"
uci -q delete firewall.wg
uci set firewall.wg="rule"
uci set firewall.wg.name="Allow-WireGuard"
uci set firewall.wg.src="wan"
uci set firewall.wg.dest_port="${VPN_PORT}"
uci set firewall.wg.proto="udp"
uci set firewall.wg.target="ACCEPT"

# Configure network
uci -q delete network.${VPN_IF}
uci set network.${VPN_IF}="interface"
uci set network.${VPN_IF}.proto="wireguard"
uci set network.${VPN_IF}.private_key="${VPN_KEY}"
uci set network.${VPN_IF}.listen_port="${VPN_PORT}"
uci add_list network.${VPN_IF}.addresses="${VPN_ADDR}"
uci add_list network.${VPN_IF}.addresses="${VPN_ADDR6}"

# Add VPN peers
uci -q delete network.wgclient
uci set network.wgclient="wireguard_${VPN_IF}"
uci set network.wgclient.public_key="${VPN_PUB}"
uci set network.wgclient.preshared_key="${VPN_PSK}"
uci add_list network.wgclient.allowed_ips="${VPN_ADDR%.*}.2/32"
uci add_list network.wgclient.allowed_ips="${VPN_ADDR6%:*}:2/128"

# SNAT and add network
uci add network alias
uci set network.@alias[-1].interface='lan'
uci set network.@alias[-1].proto='static'
uci set network.@alias[-1].ipaddr='192.168.0.21/24'
echo 'iptables -t mangle -A PREROUTING -i vpn -j MARK --set-mark 0x30' >> /etc/firewall.user
echo 'iptables -t nat -A POSTROUTING ! -o vpn -m mark --mark 0x30 -j MASQUERADE' >> /etc/firewall.user

# Reboot to test
uci commit
sync
reboot

# Login to system
ssh-keygen -f "/home/jas/moln/dot-files/ssh-known-hosts" -R "192.168.23.21"
ssh root@192.168.23.21
dmesg
logread -f &
logread
