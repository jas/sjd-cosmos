;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu))
(use-service-modules cups desktop networking ssh xorg)
(use-modules (gnu packages linux))
(use-modules (gnu services desktop))
(use-modules (gnu packages xorg))
(use-service-modules security-token)

;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
(use-package-modules security-token)

(operating-system
  (locale "sv_SE.utf8")
  (timezone "Europe/Stockholm")
  (keyboard-layout (keyboard-layout "se"))
  (host-name "paj")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "jas")
                  (comment "Simon Josefsson")
                  (group "users")
                  (home-directory "/home/jas")
                  (supplementary-groups '("wheel" "netdev" "audio" "video"
					  ;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
					  "plugdev")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (list (specification->package "nss-certs"))
                    %base-packages))

  ;; https://guix.gnu.org/en/manual/devel/en/guix.html#Name-Service-Switch
  (name-service-switch %mdns-host-lookup-nss)

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list (service gnome-desktop-service-type)
                 (service pcscd-service-type)

		 ;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
		 (udev-rules-service 'fido2 libfido2 #:groups '("plugdev"))

                 (service openssh-service-type)
                 (service cups-service-type)
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout))))

           ;; This is the default list of services we
           ;; are appending to.
           %desktop-services))
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (targets (list "/dev/nvme0n1"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "4d49fed4-3f8b-4c9d-9496-30a20a3cc797")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "2ea0981e-f125-4446-b5cd-011d09de8001"
                                  'ext4))
                         (type "ext4")) %base-file-systems)))
