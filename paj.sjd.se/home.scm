;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 (packages
  (specifications->packages
   (list
    "autoconf"
    "automake"
    "bc"
    "bison"
    "debootstrap"
    "ed"
    "emacs"
    "evolution"
    "evolution-data-server"
    "fakeroot"
    "freeipmi"
					; "fwupd"
    "gcc-toolchain"
    "gdb"
    "gengetopt"
    "gettext"
    "gimp"
    "git"
    "gnome-terminal"
    "gnome-tweaks"
    "gnupg"
    "gnutls"
    "gperf"
    "gtk-doc"
    "guile"
    "hashcash"
    "help2man"
    "htop"
    "icecat"
    "indent"
    "libassuan"
    "libheif"
    "libreoffice"
    "libtool"
    "lm-sensors"
    "lvm2"
    "make"
    "nextcloud-client"
    "password-store"
    "pbuilder"
    "perl"
    "pinentry"
    "pinentry-gnome3"
    "pkg-config"
    "po4a"
    "podman"
    "pwgen"
					; "python-yubikey-manager"
    "recutils"
    "rsync"
    "texinfo"
    "texlive"
    "virt-manager"
    )))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
		   (guix-defaults? #t)
		   (bash-profile (list (plain-file "bash-profile" "\
export PASSWORD_STORE_DIR=/home/jas/moln/password-store/
export HISTFILE=$XDG_CACHE_HOME/.bash_history
export LESSHISTFILE=$XDG_CACHE_HOME/.lesshst"))))))))
