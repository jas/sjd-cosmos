(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp))

(home-environment
 (packages
  (specifications->packages
   (list
    "git"
    "indent"
    "icecat"
    "libreoffice"
    "glibc-locales"))))
