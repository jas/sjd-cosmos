;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 (packages
  (specifications->packages
   (list
    ;; environment
    "emacs"
    "evolution"
    "evolution-data-server"
    "gimp"
    "gnome-terminal"
    "gnome-tweaks"
    "hashcash"
    "icecat"
    "libheif"
    "libreoffice"
    "nextcloud-client"
    "password-store"
    "podman"
    "polari"
    "pwgen"
    "python-yubikey-manager"
    ;; android
    "adb"
    "fastboot"
    ;; development
    "autoconf"
    "automake"
    "bison"
    "gcc-toolchain"
    "gdb"
    "gengetopt"
    "gettext"
    "git"
    "git-lfs"
    "gperf"
    "gtk-doc"
    "guile"
    "help2man"
    "indent"
    "libtool"
    "make"
    "perl"
    "pkg-config"
    "po4a"
    "recutils"
    "texinfo"
    "texlive"
    ;; debian
    "debootstrap"
    "fakeroot"
    "pbuilder"
    ;; sysadmin tools
    "bc"
    "bind:utils"
    "cryptsetup"
    "ed"
    "file"
    "freeipmi"
    "fwupd"
    "gnupg"
    "gnutls"
    "hdparm"
    "htop"
    "iotop"
    "lm-sensors"
    "lsof"
    "lvm2"
    "mdadm"
    "mergerfs"
    "neofetch"
    "net-tools"
    "nvme-cli"
    "parted"
    "rsync"
    "smartmontools"
    "strace"
    "unzip"
    "virt-manager"
    ;; gpg gnome
    "libassuan"
    "pinentry"
    "pinentry-gnome3"
    )))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
		   (guix-defaults? #t)
		   (bash-profile (list (plain-file "bash-profile" "\
export PASSWORD_STORE_DIR=/home/jas/moln/password-store/
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
export HISTFILE=$XDG_CACHE_HOME/.bash_history
export LESSHISTFILE=$XDG_CACHE_HOME/.lesshst"))))))))
