;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu))
(use-service-modules cups desktop networking ssh xorg)

;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
(use-service-modules security-token)
(use-package-modules security-token)

(operating-system
  (locale "sv_SE.utf8")
  (timezone "Europe/Stockholm")
  (keyboard-layout (keyboard-layout "se"))
  (host-name "yeti")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                  (name "jas")
                  (comment "Simon Josefsson")
                  (group "users")
                  (home-directory "/home/jas")
                  (supplementary-groups '("wheel" "netdev" "audio" "video"
					  ;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
					  "plugdev")))
                %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages %base-packages)

  ;; https://guix.gnu.org/en/manual/devel/en/guix.html#Name-Service-Switch
  (name-service-switch %mdns-host-lookup-nss)

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list (service gnome-desktop-service-type)
                 (service pcscd-service-type)

		 ;; https://guix.gnu.org/cookbook/en/html_node/Using-security-keys.html
		 (udev-rules-service 'fido2 libfido2 #:groups '("plugdev"))

                 ;; To configure OpenSSH, pass an 'openssh-configuration'
                 ;; record as a second argument to 'service' below.
                 (service openssh-service-type)
                 (service cups-service-type)
                 (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout))))

;		 (service network-manager-service-type)
;                 (service wpa-supplicant-service-type)
;                 (service ntp-service-type))

           ;; This is the default list of services we
           ;; are appending to.
           %desktop-services))
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
  (swap-devices (list (swap-space
                        (target (uuid
                                 "111831a0-2303-4375-bdfa-9888fc65f491")))))

  ;; The list of file systems that get "mounted".  The unique
  ;; file system identifiers there ("UUIDs") can be obtained
  ;; by running 'blkid' in a terminal.
  (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "D7A6-408D"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "32e75b2d-581e-4500-8f96-c39a399778cb"
                                  'ext4))
                         (type "ext4"))
		       (file-system
                        (mount-point "/home")
                        (device (uuid
                                 "bfc5cede-2e9a-47da-88fe-86e528b684d3"
                                 'ext4))
                        (type "ext4")) %base-file-systems)))
