<VirtualHost *:80>
    ServerName josefsson.org
    ServerAlias www.josefsson.org

    ErrorLog ${APACHE_LOG_DIR}/josefsson.org-error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/josefsson.org-access.log combined

    # For munin
    <Location /server-status>
	SetHandler server-status
	Order deny,allow
	Deny from all
	Allow from 127.0.0.1
    </Location>

    RewriteEngine on
    RewriteCond %{SERVER_NAME} =josefsson.org [OR]
    RewriteCond %{SERVER_NAME} =www.josefsson.org
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

<IfModule mod_ssl.c>
    <IfFile /var/lib/dehydrated/certs/josefsson.org/fullchain.pem>
	<IfFile /var/lib/dehydrated/certs/josefsson.org/privkey.pem>
	    <VirtualHost *:443>
		ServerName josefsson.org
		ServerAlias www.josefsson.org

		SSLCertificateFile /var/lib/dehydrated/certs/josefsson.org/fullchain.pem
		SSLCertificateKeyFile /var/lib/dehydrated/certs/josefsson.org/privkey.pem

		Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains"
		Header always set Content-Security-Policy "default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; upgrade-insecure-requests"
		Header always set X-Frame-Options "SAMEORIGIN"
		Header always set X-Content-Type-Options "nosniff"
		Header always set X-Permitted-Cross-Domain-Policies "none"
		Header always set Referrer-Policy "no-referrer"
		Header always set Expect-CT "enforce, max-age=30, report-uri=\"https://568196465ce39e9153d27d3dafc07f9b.report-uri.com/r/d/ct/enforce\""

		Protocols h2 http/1.1

		DocumentRoot /var/www/josefsson.org
		<Directory />
		    Options FollowSymLinks
		    AllowOverride None
		</Directory>
		<Directory /var/www/josefsson.org/>
		    Options Indexes FollowSymLinks MultiViews
		    AllowOverride None
		    Order allow,deny
		    allow from all
		</Directory>
		<Directory "/var/www/josefsson.org/.well-known/openpgpkey">
		  Options -Indexes
		  AllowOverride None
		  <IfModule mod_mime.c>
		    ForceType application/octet-stream
		  </IfModule>
		  <IfModule mod_headers.c>
		    Header always set Access-Control-Allow-Origin "*"
		  </IfModule>
		</Directory>

                RewriteEngine on
		RewriteMap lc int:tolower
		RewriteMap unescape int:unescape

		RewriteCond %{REQUEST_URI} ^/\.well-known/webfinger$
		RewriteCond ${lc:${unescape:%{QUERY_STRING}}} (?:^|&)resource=acct:simon@josefsson\.org(?:$|&)
		RewriteRule ^(.*)$ https://fosstodon.org/.well-known/webfinger?resource=acct:jas@fosstodon.org [L,R=302]

		RewriteCond %{REQUEST_URI} ^/\.well-known/host-meta$
		RewriteCond ${lc:${unescape:%{QUERY_STRING}}} (?:^|&)resource=acct:simon@josefsson\.org(?:$|&)
		RewriteRule ^(.*)$ https://fosstodon.org/.well-known/host-meta?resource=acct:jas@fosstodon.org [L,R=302]

		RewriteCond %{REQUEST_URI} ^/\.well-known/nodeinfo$
		RewriteCond ${lc:${unescape:%{QUERY_STRING}}} (?:^|&)resource=acct:simon@josefsson\.org(?:$|&)
		RewriteRule ^(.*)$ https://fosstodon.org/.well-known/nodeinfo?resource=acct:jas@fosstodon.org [L,R=302]

		ErrorLog ${APACHE_LOG_DIR}/josefsson.org-ssl-error.log
		LogLevel warn
		CustomLog ${APACHE_LOG_DIR}/josefsson.org-ssl-access.log combined
	    </VirtualHost>
	</IfFile>
    </IfFile>
</IfModule>
